## book-service

# Nama Aplikasi = Bioskop28

# Rest API = - flask = - flask digunakan karena lebih ringan dari rest API yg lain
#                      - dapat mengatur routing
#                      - akses method post
#                      - akses parameter get

#            - flask_jwt_extended = - untuk mengatur pilot dari email user
#                                   - sebagai security aplikasi 

# @jwt_required() = mengatur pilot dari email user
# if dbresult is not None: = mengambil user lebih dari satu
# @app.route() = sebagai routing aplikasi (get, post)
# json.dumps = merubah id yg d pilih menjadi sebuah json
# user = {} = di ambil dari inner join pada bagian models
# return film_data.json() = untuk membalikan ke json dengan cara mengambil data menggunakan restApi dari getFilmById

# Disini saya hanya menggunakan 2 service saja.