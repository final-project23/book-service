from app.models.views_model import database
from app.models.films_model import database as fil_db
from flask import jsonify, request
from flask_jwt_extended import *
import json, datetime, requests

mysqldb = database()
fil_db = fil_db()

@jwt_required() # bisa masuk kedalam semua dalam hal jwt # penggunaan untuk mengatur pilot dari email user
def shows():
    # ambil payload kita dari token
    params = get_jwt_identity() #akan membalikan identitas dari jwt
    dbresult = mysqldb.showViewByEmail(**params)
    result = []
    if dbresult is not None: # karena usernya lebih dari satu
        for items in dbresult:
            id = json.dumps({"id":items[4]}) # merubah id (items ke 4) yg d pilih menjadi sebuah json
            filmdetails = getFilmById(id)
            user = {
                "nama": items[0],
                "id": items[1],
                "viewdate": items[2],
                "id": items[4],
                "film_name": items[5],
                "author": filmdetails["pencipta"],
                "releaseyear": filmdetails["tahunterbit"],
                "kategori": filmdetails["kategori"]
            } # di ambil dari inner join
            result.append(user)
    else:
        result=dbresult
    return jsonify(result)

@jwt_required()
def insert(**params):
    token = get_jwt_identity()
    user = fil_db.showUserByEmail(**token)[0]
    viewdate = datetime.datetime.now().isoformat()
    id - json.dumps({"id":params["id"]})
    filmname = getFilmById(id)["nama"]
    params.update(
        {
            "id":id,
            "viewdate":viewdate,
            "film_name":film_name,
            "isactive":1
        }
    )
    mysqldb.insertView(**params)
    mysqldb.dataCommit()
    return jsonify({"message":"Success"})

@jwt_required()
def update(**params):
    mysqldb.updateView(**params)
    mysqldb.dataCommit()
    return jsonify({"message":"Success"})

def getFilmById(data):
    film_data = requests.get(url="http://localhost:8000/filmbyid", data=data)
    return film_data.json() # untuk membalikan ke json # mengambil data dengan restApi dari getFilmById