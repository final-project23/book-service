from app import app
from app.controllers import views_controller
from flask import Blueprint, request

views_blueprint = Blueprint("views_router", __name__)

@app.route("/views", methods=["GET"])
def showView():
    return views_controller.shows()

@app.route("/views/insert", methods=["POST"])
def insertView():
    params = request.json
    return views_controller.insert(**params)

@app.route("/views/status", methods=["POST"])
def updateStatus():
    params = request.json
    return views_controller.update(**params)

