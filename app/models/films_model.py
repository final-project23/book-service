from mysql.connector import connect

class database:
    def __init__(self):
        try:
            self.db = connect(host='localhost',
                              database='bioskop',
                              user='root',
                              password='')
        except Exception as e:
            print(e)

    def showUsers(self):
        cursor = self.db.cursor()
        query = '''selct * from films'''
        cursor.execute(query)
        result = cursor.fetchall()
        return result

    def showUserById(self, **params):
        cursor = self.db.cursor()
        query = '''
            select * from films where id = {0};
            '''.format(params["id"])

        cursor.execute(query)
        result = cursor.fetchone()
        return result

    def showUserByEmail(self, **params):
        cursor = self.db.cursor()
        query = '''
                select * from films where email = "{0}";
                '''.format(params["email"])

        cursor.execute(query)
        result = cursor.fetchone()
        return result

    def insertUser(self, **params):
        column = ', '.join(list(params['values'].keys()))
        values = tuple(list(params['values'].values()))
        crud_query = '''insert into films ({0}) values {1};'''.format(column, values)
        cursor =self.db.cursor()
        cursor.execute(crud_query)

    def updateUserById(self, **params):
        id = params['id']
        values = self.restructureParams(**params['values'])
        crud_query = '''update films set {0} where id = {1};'''.format(values, id)

        cursor = self.db.cursor()
        cursor.execute(crud_query)

    def deleteUserById(self, **params):
        id = params['id']
        crud_query = '''delete from films where id = {0};'''.format(id)

        cursor = self.db.cursor()
        cursor.execute(crud_query)

    def dataCommit(self):
        self.db.commit()

    def restructureParams(self, **data):
        list_data = ['{0} = "{1}"'.format(item[0],item[1]) for item in data.items()]
        result = ', '.join(list_data)
        return result
