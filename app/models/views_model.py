from mysql.connector import connect

class database:
    def __init__(self):
        try:
            self.db = connect(host='localhost',
                              database='bioskop',
                              user='root',
                              password='')
        except Exception as e:
            print(e)

    def showViewByEmail(self, **params):
        cursor =self.db.cursor()
        query = '''select films.nama, views.*
        from views
        inner join films on views.id = films.id
        where films.email = "{0}" and views.isactive = 1;
        '''.format(params["email"])
        cursor.execute(query)
        result = cursor.fetchall()
        return result

    def insertView(self, **params):
        column = ', '.join(list(params.keys()))
        values = tuple(list(params.values()))
        cursor =self.db.cursor()
        query = '''insert into views ({0})
        values {1};
        '''.format(column, values)
        print(query)
        cursor.execute(query)

    def updateView(self, **params):
        id = params["id"]
        cursor =self.db.cursor()
        query = '''update views
        set isactive = 0
        where id = {0};
        '''.format(id)
        cursor.execute(query)

    def dataCommit(self):
        self.db.commit()